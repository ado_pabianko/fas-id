<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/category-detail/{category}', [App\Http\Controllers\HomeController::class, 'categoryDetail'])->name('category.detail');
Route::get('/post-detail/{post}', [App\Http\Controllers\HomeController::class, 'postDetail'])->name('post.detail');

Auth::routes();

Route::get('/sign-in/github', [App\Http\Controllers\Auth\RegisterController::class, 'github'])->name('signin.github');
Route::get('/sign-in/github/redirect', [App\Http\Controllers\Auth\RegisterController::class, 'githubRedirect'])->name('signin.github-redirect');
Route::get('/sign-in/google', [App\Http\Controllers\Auth\RegisterController::class, 'google'])->name('signin.google');
Route::get('/sign-in/google/redirect', [App\Http\Controllers\Auth\RegisterController::class, 'googleRedirect'])->name('signin.google-redirect');
Route::get('/sign-in/facebook', [App\Http\Controllers\Auth\RegisterController::class, 'facebook'])->name('signin.facebook');
Route::get('/sign-in/facebook/redirect', [App\Http\Controllers\Auth\RegisterController::class, 'facebookRedirect'])->name('signin.facebook-redirect');
Route::get('/admin', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('admin');
Route::get('/admin/home', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('admin.home');
Route::get('/admin/change-password', [App\Http\Controllers\Admin\HomeController::class, 'changePassword'])->name('admin.change-password');
Route::post('/admin/change-password-store', [App\Http\Controllers\Admin\HomeController::class, 'changePasswordStore'])->name('admin.change-password-store');

Route::get('/admin/categories', [App\Http\Controllers\Admin\CategoriesController::class, 'index'])->name('admin.categories');
Route::get('/admin/categories/datatables', [App\Http\Controllers\Admin\CategoriesController::class, 'datatables'])->name('admin.categories.datatables');
Route::get('/admin/categories/create', [App\Http\Controllers\Admin\CategoriesController::class, 'create'])->name('admin.categories.create');
Route::post('/admin/categories/store', [App\Http\Controllers\Admin\CategoriesController::class, 'store'])->name('admin.categories.store');
Route::get('/admin/categories/{category}/edit', [App\Http\Controllers\Admin\CategoriesController::class, 'edit'])->name('admin.categories.edit');
Route::put('/admin/categories/{category}/update', [App\Http\Controllers\Admin\CategoriesController::class, 'update'])->name('admin.categories.update');
Route::get('/admin/categories/{category}/destroy', [App\Http\Controllers\Admin\CategoriesController::class, 'destroy'])->name('admin.categories.destroy');

Route::get('/admin/post', [App\Http\Controllers\Admin\PostController::class, 'index'])->name('admin.post');
Route::get('/admin/post/datatables', [App\Http\Controllers\Admin\PostController::class, 'datatables'])->name('admin.post.datatables');
Route::get('/admin/post/create', [App\Http\Controllers\Admin\PostController::class, 'create'])->name('admin.post.create');
Route::post('/admin/post/store', [App\Http\Controllers\Admin\PostController::class, 'store'])->name('admin.post.store');
Route::get('/admin/post/{post}/edit', [App\Http\Controllers\Admin\PostController::class, 'edit'])->name('admin.post.edit');
Route::put('/admin/post/{post}/update', [App\Http\Controllers\Admin\PostController::class, 'update'])->name('admin.post.update');
Route::get('/admin/post/{post}/destroy', [App\Http\Controllers\Admin\PostController::class, 'destroy'])->name('admin.post.destroy');