<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id',
        'created_by',
        'title',
        'slug',
        'short_description',
        'content',
        'image',
        'thumbnail',
    ];

    public function category() {
        return $this->belongsTo('App\Models\Categories', 'category_id');
    }
}
