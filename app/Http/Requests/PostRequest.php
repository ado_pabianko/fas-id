<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()) {
            case 'POST':
                return [
                    'title' => 'required|unique:posts',
                    'short_description' => 'required',
                    'content' => 'required',
                    'image' => 'required|image|mimes:jpeg,jpg,png|max:2000',
                    'category_id' => 'required',
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'title' => ['required', Rule::unique('posts')->ignore($this->POST('title'),'title')],
                    'short_description' => 'required',
                    'content' => 'required',
                    'image' => 'image|mimes:jpeg,jpg,png|max:2000',
                    'category_id' => 'required',
                ];
            default:break;
        }
    }
}
