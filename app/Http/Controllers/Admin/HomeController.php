<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\AdminRepository;
use App\Http\Requests\ChangePasswordRequest;

class HomeController extends Controller
{
    private $adminRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AdminRepository $adminRepository)
    {
        $this->middleware('auth');

        $this->adminRepository = $adminRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin/home');
    }

    public function changePassword() 
    {
        return view('admin.change-password');
    }

    public function changePasswordStore(ChangePasswordRequest $request) 
    {
        $userId = \Auth::user()->id;
        $oldPasswordReq = $request->old_password;
        $newPassword = $request->password;

        $oldPassword = $this->adminRepository->getOldPassword($userId);

        if (!\Hash::check($oldPasswordReq, $oldPassword->password)) {
            \Session::flash('alert-danger', 'Old password doesnt match');
        
            return redirect()->route('admin.change-password');
        }

        $changePassword = $this->adminRepository->changePasswordSave($userId, $newPassword);

        if ($changePassword) {
            \Session::flash('alert-success', 'Password successfully updated');
        } else {
            \Session::flash('alert-danger', 'Password unsuccessfully updated');
        }

        return redirect()->route('admin.change-password');
    }
}
