<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Repositories\CategoriesRepository;
use App\Repositories\PostRepository;
use App\Repositories\StorageRepository;
use App\Models\Post;

class PostController extends Controller
{
    private $cartegoryRepository;
    private $postRepository;
    private $storageRepository;

    public function __construct(
        CategoriesRepository $categoryRepository,
        PostRepository $postRepository,
        StorageRepository $storageRepository
    ) {
        $this->middleware('auth');

        $this->categoryRepository = $categoryRepository;
        $this->postRepository = $postRepository;
        $this->storageRepository = $storageRepository;
    }

    public function index() {
        return view('admin.post.index');
    }

    public function datatables() {
        return $this->postRepository->datatables();
    }

    public function create() {
        $categories = $this->categoryRepository->getAll();

        return view('admin.post.create', compact('categories'));
    }

    public function store(PostRequest $request) {
        $file = $request->file('image');
        $filename = time().'.'.$file->extension();
        $upload = $this->storageRepository->upload($file, $filename);

        if ($upload != true) {
            \Session::flash("alert-error", $upload);

            return redirect()->route('admin.post.create');
        }

        $req = $request->all();
        $slug = str_replace(' ', '-', strtolower($request->title));
        $req['slug'] = $slug;
        $req['image'] = $filename;
        $req['thumbnail'] = $filename;
        $req['created_by'] = \Auth::user()->id;

        $store = $this->postRepository->store($req);

        if ($store) {
            \Session::flash("alert-success", "Post successfully saved");
        } else {
            \Session::flash("alert-error", "Post unsuccessfully saved");
        }

        return redirect()->route('admin.post');
    }

    public function edit(Post $post) {
        $categories = $this->categoryRepository->getAll();

        return view('admin.post.edit', compact('post','categories'));
    }

    public function update(PostRequest $request, Post $post) {
        $req = $request->all();
        
        if ($request->file('image')) {
            $file = $request->file('image');
            $oldFilename = $post->image;
            $newFilename = time().'.'.$file->extension();
            $req['image'] = $newFilename;
            $req['thumbnail'] = $newFilename;

            $upload = $this->storageRepository->upload($file, $newFilename, $oldFilename);

            if ($upload != true) {
                \Session::flash("alert-error", $upload);
    
                return redirect()->route('post.edit', ['post' => $post->id]);
            }
        } else {
            unset($req['image']);
            unset($req['thumbnail']); 
        }

        $slug = str_replace(' ', '-', strtolower($request->title));
        $req['slug'] = $slug;
        $req['created_by'] = \Auth::user()->id;

        $update = $this->postRepository->update($req, $post);

        if ($update) {
            \Session::flash("alert-success", "Post successfully updated");
        } else {
            \Session::flash("alert-error", "Post unsuccessfully updated");
        }

        return redirect()->route('admin.post');
    }

    public function destroy(Post $post) {
        $destroy = $this->postRepository->destroy($post->id);

        if ($destroy) {
            \Session::flash("alert-success", "Post successfully destroyed");
        } else {
            \Session::flash("alert-error", "Post unsuccessfully destroyed");
        }

        $this->storageRepository->deleteFile($post->image);

        return redirect()->route('admin.post');
    }
}
