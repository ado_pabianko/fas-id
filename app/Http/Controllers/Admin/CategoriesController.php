<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CategoriesRequest;
use App\Repositories\CategoriesRepository;
use App\Models\Categories;

class CategoriesController extends Controller
{
    private $categoryRepository;

    public function __construct(CategoriesRepository $categoryRepository) {
        $this->middleware('auth');

        $this->categoryRepository = $categoryRepository;
    }

    public function index() {
        return view('admin.categories.index');
    }

    public function datatables() {
        return $this->categoryRepository->datatables();
    }

    public function create() {
        return view('admin.categories.create');
    }

    public function store(CategoriesRequest $request) {
        $req['name'] = $request->name; 
        $slug = str_replace(' ', '-', strtolower($request->name));
        $req['slug'] = $slug;

        $store = $this->categoryRepository->store($req);

        if ($store) {
            \Session::flash("alert-success", "Category successfully saved");
        } else {
            \Session::flash("alert-error", "Category unsuccessfully saved");
        }

        return redirect()->route('admin.categories');
    }

    public function edit(Categories $category) {
        return view('admin.categories.edit', compact('category'));
    }

    public function update(CategoriesRequest $request, Categories $category) {
        $req['name'] = $request->name; 
        $slug = str_replace(' ', '-', strtolower($request->name));
        $req['slug'] = $slug;

        $update = $this->categoryRepository->update($req, $category);

        if ($update) {
            \Session::flash("alert-success", "Category successfully updated");
        } else {
            \Session::flash("alert-error", "Category unsuccessfully updated");
        }

        return redirect()->route('admin.categories');
    }

    public function destroy(Categories $category) {
        $destroy = $this->categoryRepository->destroy($category->id);

        if ($destroy) {
            \Session::flash("alert-success", "Category successfully destroyed");
        } else {
            \Session::flash("alert-error", "Category unsuccessfully destroyed");
        }

        return redirect()->route('admin.categories');
    }
}
