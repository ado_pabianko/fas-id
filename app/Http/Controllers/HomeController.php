<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CategoriesRepository;
use App\Repositories\PostRepository;
use App\Models\Post;

class HomeController extends Controller
{
    private $categoriesRepository;
    private $postRepository;

    public function __construct(
        CategoriesRepository $categoryRepository, 
        PostRepository $postRepository
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->postRepository = $postRepository;
    }

    public function index() {
        $categories = $this->categoryRepository->getAll();
        $posts = $this->postRepository->getAllWithPagination();

        return view('home', compact('categories', 'posts'));
    }

    public function postDetail($slug) {
        $post = $this->postRepository->findBySlug($slug);
        $categories = $this->categoryRepository->getAll();

        return view('post-detail', compact('categories', 'post'));
    }

    public function categoryDetail($slug) {
        $posts = $this->postRepository->findBySlugCategory($slug);
        $categories = $this->categoryRepository->getAll();
        // dd($posts);

        return view('home', compact('categories', 'posts'));
    }
}
