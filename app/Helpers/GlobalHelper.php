<?php

use App\Models\User;
use App\Models\Categories;

if (!function_exists('get_userdata')) {
    function get_userdata($userId) {
        return User::findOrFail($userId);
    }
}

if (!function_exists('get_category')) {
    function get_category($categoryId) {
        return Categories::findOrFail($categoryId);
    }
}