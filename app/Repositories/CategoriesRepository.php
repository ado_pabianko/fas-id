<?php

namespace App\Repositories;

use App\Repositories\Interfaces\CategoriesRepositoryInterface;
use App\Models\Categories;
use Yajra\Datatables\Datatables;

class CategoriesRepository implements CategoriesRepositoryInterface {
    public function getAll() {
        return Categories::all();
    }

    public function datatables() {
        return Datatables::of(Categories::query())
        ->editColumn('actions', function($col) {
            $actions = '';

            $actions .= '
                <a href="'.route('admin.categories.edit', ['category' => $col->id]).'" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Edit">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                </a>
            ';

            $actions .= '
                <a href="'.route('admin.categories.destroy', ['category' => $col->id]).'" class="btn btn-sm btn-danger" onclick="return confirm(\'Anda yakin untuk menghapus data ?\');" data-toggle="tooltip" data-placement="top" title="Delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                </a>
            ';

            return $actions;
        })
        ->rawColumns(['actions'])
        ->addIndexColumn()
        ->make(true);
    }

    public function store($categoryData) {
        $category = new Categories($categoryData);

        return $category->save();
    }

    public function update($reqParam, $categoryData) {
        return $categoryData->update($reqParam);
    }

    public function destroy($categoryId) {
        return Categories::destroy($categoryId);
    }
}