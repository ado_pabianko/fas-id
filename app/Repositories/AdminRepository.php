<?php

namespace App\Repositories;

use App\Repositories\Interfaces\AdminRepositoryInterface;
use App\Models\User;

class AdminRepository implements AdminRepositoryInterface {
    public function getOldPassword($userId) {
        return User::select('password')->where('id', $userId)->first();
    }

    public function changePasswordSave($userId, $newPassword) {
        return User::where('id', $userId)->update(['password' => \bcrypt($newPassword)]);
    }
}