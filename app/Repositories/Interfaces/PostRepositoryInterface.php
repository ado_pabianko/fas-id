<?php

namespace App\Repositories\Interfaces;

interface PostRepositoryInterface {
    public function getAll();
    public function getAllWithPagination();
    public function findBySlug($slug);
    public function findBySlugCategory($slug);
    public function datatables();
    public function store($categoryData);
    public function update($reqParam, $categoryData);
    public function destroy($categoryId);
}
