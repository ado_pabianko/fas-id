<?php

namespace App\Repositories\Interfaces;

interface AdminRepositoryInterface {
    public function getOldPassword($userId);
    public function changePasswordSave($userId, $newPassword);
}