<?php

namespace App\Repositories\Interfaces;

interface StorageRepositoryInterface {
    public function upload($file, $filename, $oldFilename);
    public function deleteFile($filename);
}