<?php

namespace App\Repositories\Interfaces;

interface CategoriesRepositoryInterface {
    public function getAll();
    public function datatables();
    public function store($categoryData);
    public function update($reqParam, $categoryData);
    public function destroy($categoryId);
}