<?php

namespace App\Repositories;

use App\Repositories\Interfaces\PostRepositoryInterface;
use App\Models\Post;
use Yajra\Datatables\Datatables;

class PostRepository implements PostRepositoryInterface {
    public function getAll() {
        return Post::orderBy('id', 'desc')->get();
    }

    public function getAllWithPagination() {
        return Post::select('category_id', 'created_by', 'title', 'slug', 'short_description', 'thumbnail', 'created_at')
        ->orderBy('id', 'desc')->simplePaginate(10);
    }

    public function findBySlug($slug) {
        return Post::where('slug', $slug)->first();
    }

    public function findBySlugCategory($slug) {
        return \DB::table('posts as aa')
        ->select('aa.category_id', 'aa.created_by', 'aa.title', 'aa.slug', 'aa.short_description', 'aa.thumbnail', 'aa.created_at')
        ->join('categories as bb', 'aa.category_id', '=', 'bb.id')
        ->where('bb.slug', $slug)
        ->orderBy('aa.id', 'desc')->simplePaginate(10);
    }

    public function datatables() {
        return Datatables::of(Post::query())
        ->editColumn('actions', function($col) {
            $actions = '';

            $actions .= '
                <a href="'.route('admin.post.edit', ['post' => $col->id]).'" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Edit">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                </a>
            ';

            $actions .= '
                <a href="'.route('admin.post.destroy', ['post' => $col->id]).'" class="btn btn-sm btn-danger" onclick="return confirm(\'Anda yakin untuk menghapus data ?\');" data-toggle="tooltip" data-placement="top" title="Delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                </a>
            ';

            return $actions;
        })
        ->editColumn('category', function($col) {
            return get_category($col->category_id)->name;
        })
        ->editColumn('created_by', function($col) {
            return get_userdata($col->created_by)->name;
        })
        ->rawColumns(['actions'])
        ->addIndexColumn()
        ->make(true);
    }

    public function store($postData) {
        $post = new Post($postData);

        return $post->save();
    }

    public function update($reqParam, $postData) {
        return $postData->update($reqParam);
    }

    public function destroy($postId) {
        return Post::destroy($postId);
    }
}