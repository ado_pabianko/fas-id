<?php

namespace App\Repositories;

use App\Repositories\Interfaces\StorageRepositoryInterface;

class StorageRepository implements StorageRepositoryInterface {
    public function upload($file, $filename, $oldFilename = '') {
        try{
            $path = public_path('post');
            \Image::make($file)->save($path.'/'.$filename);

            // Thumbnail
            $pathThumb = public_path('post/thumbnail');
            $imgThumb = \Image::make($file);
            $imgThumb->resize(200,200,function($constraint) {
                $constraint->aspectRatio();
            })->save($pathThumb.'/'.$filename);

            if ($oldFilename) {
                @unlink($path.'/'.$oldFilename);
                @unlink($pathThumb.'/'.$oldFilename);
            }

            return true;
        } catch(Exception $e) {
            return $e;
        }
    }

    public function deleteFile($filename) {
        $path = public_path('post');
        $pathThumb = public_path('post/thumbnail');

        @unlink($path.'/'.$filename);
        @unlink($pathThumb.'/'.$filename);
    }
}