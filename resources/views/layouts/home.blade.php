<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <style>
            .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            }
            @media (min-width: 768px) {
            .bd-placeholder-img-lg {
            font-size: 3.5rem;
            }
            }
        </style>
        <!-- Custom styles for this template -->
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="{{ asset('css/blog.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <header class="blog-header py-3">
                <div class="row flex-nowrap justify-content-between align-items-center">
                    <div class="col-4 pt-1">
                    </div>
                    <div class="col-4 text-center">
                        <a class="blog-header-logo text-dark" href="#">{{ config('app.name') }}</a>
                    </div>
                    <div class="col-4 d-flex justify-content-end align-items-center">
                        <a class="p-2 text-muted" href="{{ route('login') }}">Login</a>
                        <a class="p-2 text-muted" href="{{ route('register') }}">Register</a>
                    </div>
                </div>
            </header>
            <div class="nav-scroller py-1 mb-2 border-bottom">
                <nav class="nav d-flex justify-content-between">
                    <a class="p-2 text-muted" href="{{ route('home') }}">Home</a>
                </nav>
            </div>
        </div>
        <br>
        <br>
        @yield('content')
        <!-- /.container -->
    </body>
</html>