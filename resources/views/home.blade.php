@extends('layouts.home')

@section('content')
<main role="main" class="container">
    <div class="row">
        <div class="col-md-8 blog-main">
            <!-- /.blog-post -->
            @foreach($posts as $item)
            <div class="blog-post">
                <h2 class="blog-post-title"><a href="{{ route('post.detail', ['post' => $item->slug]) }}">{{ $item->title }}</a></h2>
                <p class="blog-post-meta">
                    {{ date('d', strtotime($item->created_at)) }}
                    {{ date('F', strtotime($item->created_at)) }}
                    {{ date('Y', strtotime($item->created_at)) }}
                    by {{ get_userdata($item->created_by)->name}}
                    - {{ get_category($item->category_id)->name }}
                </p>
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="{{ asset('post/thumbnail/'.$item->thumbnail) }}" class="card-img" alt="">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body" style="padding-top: 0">
                            {!! $item->short_description !!}
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

            <nav class="blog-pagination">
                {{ $posts->links() }}
            </nav>
        </div>
        <!-- /.blog-main -->
        <aside class="col-md-4 blog-sidebar">
            <div class="p-4">
                <h4 class="font-italic">Categories</h4>
                <ol class="list-unstyled mb-0">
                    @foreach($categories as $item)
                    <li><a href="{{ route('category.detail', ['category' => $item->slug]) }}">{{ $item->name }}</a></li>
                    @endforeach
                </ol>
            </div>
        </aside>
        <!-- /.blog-sidebar -->
    </div>
    <!-- /.row -->
</main>
@stop