@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Form Edit Post') }}</div>

                <div class="card-body">
                    @foreach (['danger', 'success'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <div class="alert alert-{{ $msg }} alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ Session::get('alert-' . $msg) }}
                            </div>
                        @endif
                    @endforeach
                    <form method="POST" action="{{ route('admin.post.update', ['post' => $post]) }}" enctype="multipart/form-data">
                        
                        @method('PUT')
                        @csrf

                        <div class="form-group row">
                            <label for="title" class="col-md-3 col-form-label text-md-right">{{ __('Title') }} <label class="text-danger">*</label></label>

                            <div class="col-md-8">
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ $post->title }}" required autocomplete="title">

                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="short-description" class="col-md-3 col-form-label text-md-right">{{ __('Short Description') }} <label class="text-danger">*</label></label>

                            <div class="col-md-8">
                                <textarea id="short-description" class="form-control ckeditor @error('short_description') is-invalid @enderror" name="short_description" required>{{ $post->short_description }}</textarea>

                                @error('short_description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="content" class="col-md-3 col-form-label text-md-right">{{ __('Content') }} <label class="text-danger">*</label></label>

                            <div class="col-md-8">
                                <textarea id="content" class="form-control @error('content') is-invalid @enderror" name="content" required>{{ $post->content }}</textarea>

                                @error('content')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="image" class="col-md-3 col-form-label text-md-right">{{ __('Image') }}</label>

                            <div class="col-md-8">
                                <img id="img-preview" class="hidden" src="{{ asset('post/thumbnail/'.$post->thumbnail) }}" style="max-width: 150px"/><br>
                                <input id="image" type="file" class="@error('image') is-invalid @enderror" name="image">

                                @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="category-id" class="col-md-3 col-form-label text-md-right">{{ __('Category') }} <label class="text-danger">*</label></label>

                            <div class="col-md-8">
                                <select id="category-id" class="form-control @error('category_id') is-invalid @enderror" name="category_id" required>
                                    <option disabled>Choose</option>
                                    @foreach($categories as $item)
                                    @php $selected = $item->id == $post->category_id ? 'selected' : '' @endphp
                                    <option {{ $selected }} value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>

                                @error('category_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-3">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
<script>
    $(function() {
        CKEDITOR.replace('content');

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $("#img-preview").removeClass("hidden");
                    $("#img-preview").attr("src", e.target.result);
                }

                reader.readAsDataURL(input.files[0]) // convert to base64 sting
            }
        }

        $("#image").change(function() {
            readURL(this);
        })
    })
</script>
@stop