@extends('layouts.home')

@section('content')
<main role="main" class="container">
    <div class="row">
        <div class="col-md-8 blog-main">
            <!-- /.blog-post -->
            <div class="blog-post">
                <h2 class="blog-post-title">{{ $post->title }}</h2>
                <p class="blog-post-meta">
                    {{ date('d', strtotime($post->created_at)) }}
                    {{ date('F', strtotime($post->created_at)) }}
                    {{ date('Y', strtotime($post->created_at)) }}
                    by {{ get_userdata($post->created_by)->name}}
                    - {{ get_category($post->category_id)->name }}
                </p>
                <div class="row no-gutters">
                    <img src="{{ asset('post/'.$post->image) }}" class="card-img" style="margin-bottom: 20px">
                    {!! $post->content !!}
                </div>
            </div>
        </div>
        <!-- /.blog-main -->
        <aside class="col-md-4 blog-sidebar">
            <div class="p-4">
                <h4 class="font-italic">Categories</h4>
                <ol class="list-unstyled mb-0">
                    @foreach($categories as $item)
                    <li><a href="{{ route('category.detail', ['category' => $item->slug]) }}">{{ $item->name }}</a></li>
                    @endforeach
                </ol>
            </div>
        </aside>
        <!-- /.blog-sidebar -->
    </div>
    <!-- /.row -->
</main>
@stop