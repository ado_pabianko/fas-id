<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Categories
        \DB::table('categories')->insert([
            [
                'name' => 'Finance',
                'slug' => 'finance'
            ],
            [
                'name' => 'Tekno',
                'slug' => 'tekno'
            ],
        ]);

        // Posts
        \DB::table('posts')->insert([
            [
                'category_id' => 1,
                'created_by' => 1,
                'title' => 'Jauhi Rp 1 Juta/Gram, Harga Emas Sepekan Anjlok Rp 21 Ribu',
                'slug' => 'jauhi-rp-1-juta/gram,-harga-emas-sepekan-anjlok-rp-21-ribu',
                'short_description' => '<p><strong>Jakarta</strong> -<a href="https://www.detik.com/tag/harga-emas"> Harga emas</a> sepekan terakhir terus turun meski dalam beberapa hari ada sempat merangkak naik. <a href="https://www.detik.com/tag/harga-emas-antam">Harga emas Antam</a> turun Rp 21.000/gram dalam sepekan.</p>

                <p>Harga logam mulia atau emas batangan Antam pada awal pekan, Senin (9/11) sempat mengalami kenaikan sebesar Rp 2.000 per gram menjadi Rp 1.006.000 per gram. Harga buyback atau pembelian kembali emas Antam juga naik Rp 2.000 ke level Rp 899.000/gram.</p>',
                'content' => '<p><strong>Jakarta</strong> -<a href="https://www.detik.com/tag/harga-emas"> Harga emas</a> sepekan terakhir terus turun meski dalam beberapa hari ada sempat merangkak naik. <a href="https://www.detik.com/tag/harga-emas-antam">Harga emas Antam</a> turun Rp 21.000/gram dalam sepekan.</p>

                <p>Harga logam mulia atau emas batangan Antam pada awal pekan, Senin (9/11) sempat mengalami kenaikan sebesar Rp 2.000 per gram menjadi Rp 1.006.000 per gram. Harga buyback atau pembelian kembali emas Antam juga naik Rp 2.000 ke level Rp 899.000/gram.</p>
                
                <p>Lalu, pada Selasa (10/11) harga emas turun drastis. Harga logam mulia hari itu berada di level Rp 972.000 per gram. Angka tersebut terkontraksi hingga Rp 34.000 dibandingkan kemarin. Harga buyback atau pembelian kembali emas Antam hari itu juga turun Rp 42.000 ke level Rp 857.000/gram.</p>
                
                <p>Memasuki hari berikutnya, Rabu (12/4) harga emas juga mengalami penurunan drastis. Harga logam mulia hari itu berada di level Rp 1.026.000 per gram. Angka tersebut turun Rp 30.000 dibandingkan harga Selasa.</p>
                
                <p>Selanjutnya, Kamis (13/4) harga logam mulia turun tipis Rp 2.000 dibanding kemarin menjadi Rp 970.000 per gram.</p>
                
                <p>Lalu, pada Jumat (14/4) harga emas turun lagi hingga berada di level Rp 968.000 per gram. Angka tersebut naik Rp 2.000 dibandingkan harga sebelumnya.</p>
                
                <p>Harga emas batangan tersebut sudah termasuk PPh 22 sebesar 0,9%. Bila ingin mendapatkan potongan pajak lebih rendah, yaitu sebesar 0,45% maka bawa NPWP saat transaksi.</p>',
                'image' => '1605334251.jpeg',
                'thumbnail' => '1605334251.jpeg',
            ],
            [
                'category_id' => 1,
                'created_by' => 1,
                'title' => 'Puluhan Miliarder Jadi Bekingan Joe Biden di Pilpres AS',
                'slug' => 'puluhan-miliarder-jadi-bekingan-joe-biden-di-pilpres-as',
                'short_description' => '<p><strong>Jakarta</strong> - Di balik keberhasilan <a href="https://www.detik.com/tag/joe-biden">Joe Biden</a> memenangi Pilpres AS 2020 ternyata ada sederet miliarder yang menopangnya. Capres dari Partai Demokrat ini berhasil jadi Presiden Amerika Serikat setelah mengalahkan calon Presiden petahana <a href="https://www.detik.com/tag/donald-trump">Donald Trump</a>.</p>

                <p>Melansir <em>Forbes</em>, pada Senin (9/11/2020), sejak Juli lalu ternyata ada lebih dari 70 miliarder yang memberikan dukungan dana kepada komite kampanye yang mendukung mantan Wakil Presiden di zaman Barrack Obama ini.</p>',
                'content' => '<p><strong>Jakarta</strong> - Di balik keberhasilan <a href="https://www.detik.com/tag/joe-biden">Joe Biden</a> memenangi Pilpres AS 2020 ternyata ada sederet miliarder yang menopangnya. Capres dari Partai Demokrat ini berhasil jadi Presiden Amerika Serikat setelah mengalahkan calon Presiden petahana <a href="https://www.detik.com/tag/donald-trump">Donald Trump</a>.</p>

                <p>Melansir <em>Forbes</em>, pada Senin (9/11/2020), sejak Juli lalu ternyata ada lebih dari 70 miliarder yang memberikan dukungan dana kepada komite kampanye yang mendukung mantan Wakil Presiden di zaman Barrack Obama ini.</p>
                
                <p>You may also like</p>
                
                <p>Di sisi lain, hanya ada 27 miliarder yang menyumbang untuk upaya pemilihan kembali Trump.</p>
                
                <p>Bahkan, dari 5 orang miliarder paling besar yang menjadi pendukung Biden punya jumlah kekayaan gabungan US$ 160,4 miliar atau sekitar Rp 2.216 triliun (kurs Rp 14.100)</p>
                
                <p>Berikut ini adalah daftar 5 pendukung terkaya Joe Biden dan kontribusi politik mereka pada pemilihan federal berdasarkan data Komisi Pemilihan Federal per 14 Oktober 2020:</p>
                
                <p><strong>1. Steve &amp; Connie Ballmer</strong><br />
                Kekayaan bersih: US$ 69,7 miliar<br />
                Donasi untuk komite dan super-PAC yang mendukung Biden: US$ 500.000 untuk gerakan Unite the Country, US$ 5.600 untuk gerakan Biden for President, dan US$ 100.000 untuk Biden Victory Fund.</p>
                
                <p><strong>2. Jacqueline Mars</strong><br />
                Kekayaan bersih: US$ 28,9 miliar<br />
                Donasi untuk komite dan super-PAC yang mendukung Biden: US$ 500 untuk gerakan Biden for President.</p>
                
                <p><strong>3. Jim &amp; Marilyn Simons</strong><br />
                Kekayaan bersih: US$ 23,5 miliar<br />
                Donasi untuk komite dan super-PAC yang mendukung Biden: US$ 2.800 untuk gerakan Biden for President, US$ 978.400 untuk Biden Action Fund, US$ 100.000 untuk Biden Victory Fund, US$ 3 juta untuk gerakan Unite the Country, dan US$ 4 juta untuk gerakan Priority USA Action.</p>
                
                <p><strong>4. Leonard Lauder &amp; Judith Glickman Lauder</strong><br />
                Kekayaan bersih: US$ 19,7 miliar<br />
                Donasi kepada komite dan super-PAC yang mendukung Biden: US$ 2.800 untuk gerakan Biden for President.</p>
                
                <p><strong>5. Laurene Powell Jobs</strong><br />
                Kekayaan bersih: US$ 18,6 miliar<br />
                Donasi untuk komite dan super-PAC yang mendukung Biden: US$ 2.800 untuk gerakan Biden for President, dan US$ 710.600 untuk Biden Victory Fund.</p>',
                'image' => '1605334339.jpeg',
                'thumbnail' => '1605334339.jpeg',
            ],
            [
                'category_id' => 1,
                'created_by' => 1,
                'title' => 'Dear Para Sultan, Belanja Dong Biar RI Keluar dari Jurang Resesi',
                'slug' => 'dear-para-sultan,-belanja-dong-biar-ri-keluar-dari-jurang-resesi',
                'short_description' => '<p><strong>Jakarta</strong> - Tingkat konsumsi rumah tangga menjadi salah satu cara mengangkat Indonesia <a href="https://www.detik.com/tag/resesi">keluar dari jurang resesi</a>. konsumsi rumah tangga memiliki kontribusi terbesar dalam pembentukan <a href="https://www.detik.com/tag/pertumbuhan-ekonomi">produk domestik bruto (PDB)</a>.</p>

                <p>Itu artinya, jika konsumsi rumah tangga positif maka ekonomi nasional pun akan pulih kembali. Peneliti dari Center of Reform on Economics (CORE) Indonesia, Yusuf Rendy Manilet mengatakan kelompok menengah atas alias orang kaya di Indonesia sampai saat ini masih menahan belanja karena khawatir dengan kasus COVID-19 di tanah air.</p>',
                'content' => '<p><strong>Jakarta</strong> - Tingkat konsumsi rumah tangga menjadi salah satu cara mengangkat Indonesia <a href="https://www.detik.com/tag/resesi">keluar dari jurang resesi</a>. konsumsi rumah tangga memiliki kontribusi terbesar dalam pembentukan <a href="https://www.detik.com/tag/pertumbuhan-ekonomi">produk domestik bruto (PDB)</a>.</p>

                <p>Itu artinya, jika konsumsi rumah tangga positif maka ekonomi nasional pun akan pulih kembali. Peneliti dari Center of Reform on Economics (CORE) Indonesia, Yusuf Rendy Manilet mengatakan kelompok menengah atas alias orang kaya di Indonesia sampai saat ini masih menahan belanja karena khawatir dengan kasus COVID-19 di tanah air.</p>
                
                <p>&quot;Kelompok ini akhirnya menahan untuk melakukan konsumsi, artinya kelompok ini mempunyai privilege untuk tetap di rumah,&quot; kata Yusuf saat dihubungi detikcom, Jakarta, Sabtu (14/11/2020).</p>
                
                <p><a href="https://www.detik.com/tag/bps">Badan Pusat Statistik (BPS)</a> mengumumkan tingkat konsumsi rumah tangga berada di level minus 4% pada kuartal III-2020. Angka tersebut membaik dibandingkan pada kuartal sebelumnya yang minus 5%.</p>
                
                <p>Yusuf mengatakan, kelompok menengah atas atau 20% teratas ini memiliki kontribusi terhadap konsumsi cukup besar yaitu sekitar 40%. Oleh karena itu, kegiatan berbelanja atau berkonsumsi kembali bisa memberikan dampak besar terhadap pemulihan ekonomi nasional.</p>
                
                <p>&quot;Proporsi persentase kelompok ini terhadap total pengeluaran mencapai 40%. Jadi cukup besar untuk kemudian mempengaruhi konsumsi rumah tangga,&quot; jelasnya.</p>',
                'image' => '1605334458.jpeg',
                'thumbnail' => '1605334458.jpeg',
            ],
            [
                'category_id' => 1,
                'created_by' => 1,
                'title' => 'Mau Pulihkan Resesi? Kelas Menengah Jangan Pelit Belanja Ya!',
                'slug' => 'mau-pulihkan-resesi?-kelas-menengah-jangan-pelit-belanja-ya!',
                'short_description' => '<p><strong>Jakarta</strong> - Selama <a href="https://www.detik.com/tag/pandemi-corona">pandemi Corona</a> melanda, kelas menengah di Indonesia dinilai menahan uangnya untuk belanja. Hal itu terbukti dari jumlah tabungan masyarakat kelas menengah ke atas di perbankan tumbuh pesat sedangkan jumlah kredit turun.</p>

                <p>Jumlah tabungan mereka, menurut Sekretaris Eksekutif I Komite Penanganan <a href="https://www.detik.com/tag/covid_19">COVID-19</a> dan Pemulihan Ekonomi Nasional (Komite PC-PEN), Raden Pardede, hampir Rp 300 triliun.</p>',
                'content' => '<p><strong>Jakarta</strong> - Selama <a href="https://www.detik.com/tag/pandemi-corona">pandemi Corona</a> melanda, kelas menengah di Indonesia dinilai menahan uangnya untuk belanja. Hal itu terbukti dari jumlah tabungan masyarakat kelas menengah ke atas di perbankan tumbuh pesat sedangkan jumlah kredit turun.</p>

                <p>Jumlah tabungan mereka, menurut Sekretaris Eksekutif I Komite Penanganan <a href="https://www.detik.com/tag/covid_19">COVID-19</a> dan Pemulihan Ekonomi Nasional (Komite PC-PEN), Raden Pardede, hampir Rp 300 triliun.</p>
                
                <p>&quot;Itu karena mereka tidak berbelanja barang konsumsi maupun berinvestasi. Mereka sepertinya menunggu sampai pandemi bisa diatasi,&quot; kata Raden Pardede, Jumat (6/11/2020).</p>
                
                <p>Tapi melihat kecenderungannya ke depan, mereka akan mulai berani ke luar rumah dan membelanjakan uangnya. Hal itu terjadi apabila vaksin Corona sudah benar-benar teruji keamanan dan khasiatnya, serta bisa digunakan secara luas.</p>
                
                <p>Mantan konsultan Bank Dunia dan Asia Development Bank itu lantas merujuk perkataan ekonom Inggris, John Maynard Keynes. Dalam situasi resesi kalau kelas menengah terlalu berhemat perekonomian nasional akan jatuh pada depresi.</p>
                
                <p>&quot;Jadi, kami imbau kelompok kelas menengah atas ini jangan terlalu berhemat, bahaya. Bersikap lah rasional dan dengan mematuhi protokol kesehatan aktivitas belanja dan wisata bisa dilakukan kok,&quot; kata Raden Pardede.</p>
                
                <p>Di sisi lain, Raden Pardede memastikan pemerintah akan melanjutkan berbagai program perlindungan sosial agar di kuartal IV pertumbuhan ekonomi bisa lebih baik dari kuartal III yang minus 3,49%.</p>
                
                <p>Khusus untuk UMKM, selain sudah ada bantuan sosial produktif, juga ada Kredit Usaha Rakyat, dan program lainnya.</p>
                
                <p>&quot;Sampai akhir Oktober serapan anggaran COVID sudah 86-87 persen, dan serapan belanja pemerintah bisa 100 persen pada akhir 2020,&quot; kata Raden Pardede.</p>',
                'image' => '1605334535.jpeg',
                'thumbnail' => '1605334535.jpeg',
            ],
            [
                'category_id' => 1,
                'created_by' => 1,
                'title' => 'Kacab Maybank Pakai Duit Winda yang Raib buat Main Forex',
                'slug' => 'kacab-maybank-pakai-duit-winda-yang-raib-buat-main-forex',
                'short_description' => '<p><strong>Jakarta</strong> - Kuasa hukum <a href="https://www.detik.com/tag/maybank">Maybank</a> <a href="https://www.detik.com/tag/hotman-paris">Hotman Paris Hutapea</a> menyebut jika kepala cabang Maybank menggunakan uang <a href="https://www.detik.com/tag/winda-earl">Winda &#39;Earl&#39; Lunardi</a> untuk bermain forex dan berbisnis.</p>

                <p>Dalam acara Prime Talk di sebuah stasiun TV Hotman menyebutkan hilangnya uang Winda sebesar Rp 22 miliar ini karena adanya praktik terselubung yakni bank dalam bank.</p>',
                'content' => '<p><strong>Jakarta</strong> -</p>

                <p>Kuasa hukum <a href="https://www.detik.com/tag/maybank">Maybank</a> <a href="https://www.detik.com/tag/hotman-paris">Hotman Paris Hutapea</a> menyebut jika kepala cabang Maybank menggunakan uang <a href="https://www.detik.com/tag/winda-earl">Winda &#39;Earl&#39; Lunardi</a> untuk bermain forex dan berbisnis.</p>
                
                <p>Dalam acara Prime Talk di sebuah stasiun TV Hotman menyebutkan hilangnya uang Winda sebesar Rp 22 miliar ini karena adanya praktik terselubung yakni bank dalam bank.</p>
                
                <p>&quot;Diakui bahwa ada satu pimpinan cabang bank yang melakukan praktik perbankan dalam perbankan, yaitu memakai uang nasabah untuk berbisnis, dan dia tidak kabur, dia di situ memakai uang nasabah,&quot; ucapnya.</p>
                
                <p>Dia juga mengungkapkan, oknum kacab Maybank itu juga melakukan praktik trading di instrumen forex</p>
                
                <p>Hotman justru merasa aneh, ketika kacab <a href="https://www.detik.com/tag/maybank">Maybank</a> itu menggunakan uang namun tidak kabur.</p>
                
                <p>&quot;Sehingga menjadi pertanyaan adalah apakah dia memakai uang nasabah itu dengan persetujuan dari nasabah untuk mendapatkan untung yang lebih besar, karena sebagian uang tersebut dipakai untuk bermain forex. Kami tidak menuduh pemilik rekening ikut membantu,&quot; tuturnya.</p>
                
                <p>Hotman juga menjelaskan, berdasarkan hasil diskusinya dengan Head of National Anti Fraud Maybank, Nehemia Andiko, dalam proses pembukaan rekening ditandatangani oleh Winda.</p>
                
                <p>&quot;Bahkan di dalam surat yang ditandatangani oleh Winda, dia mengaku sudah menerima buku tabungan dan ATM, tapi itu tetap dipegang oleh pimpinan cabang. Yang kedua, yang dibuka bukan rekening rekening koran tapi buku tabungan, itu kata Andiko juga,&quot; ucapnya.</p>',
                'image' => '1605334653.jpeg',
                'thumbnail' => '1605334653.jpeg',
            ],
            [
                'category_id' => 1,
                'created_by' => 1,
                'title' => 'Jangan Lagi Jadi Generasi Sandwich, Ini Tipsnya!',
                'slug' => 'jangan-lagi-jadi-generasi-sandwich,-ini-tipsnya!',
                'short_description' => '<p><strong>Jakarta</strong> - Pasti sudah sering dengan istilah Generasi Sandwich dong. Saya pribadi sudah menulis tentang ini sejak 15 tahun yang lalu di pertengahan tahun 2000-an. Yes, saya sering membahas sesuatu lebih cepat dari masanya. Istilah ini sendiri pertama kali diperkenalkan oleh Dorothy A. Miller, pada 1981.<br />
                <br />
                Profesor sekaligus direktur praktikum Universitas Kentucky, Lexington, Amerika Serikat (AS), itu memperkenalkan istilah generasi sandwich dalam jurnal berjudul &quot;The &#39;Sandwich&#39; Generation: Adult Children of the Aging.&quot;</p>',
                'content' => '<p><strong>Jakarta</strong> - Pasti sudah sering dengan istilah Generasi Sandwich dong. Saya pribadi sudah menulis tentang ini sejak 15 tahun yang lalu di pertengahan tahun 2000-an. Yes, saya sering membahas sesuatu lebih cepat dari masanya. Istilah ini sendiri pertama kali diperkenalkan oleh Dorothy A. Miller, pada 1981.<br />
                <br />
                Profesor sekaligus direktur praktikum Universitas Kentucky, Lexington, Amerika Serikat (AS), itu memperkenalkan istilah generasi sandwich dalam jurnal berjudul &quot;The &#39;Sandwich&#39; Generation: Adult Children of the Aging.&quot;</p>
                
                <p><br />
                Dalam jurnal tersebut, Dorothy mendeskripsikan generasi sandwich sebagai generasi orang dewasa saat ini yang harus menanggung hidup orang tua &amp; juga anak mereka. Studi demografis menyatakan bahwa ada sekitar 47% orang dewasa berusia 40-50 tahun yang terjebak generasi sandwich.</p>
                
                <table>
                    <tbody>
                        <tr>
                            <td>
                            <p><strong>Baca juga: </strong><a href="https://finance.detik.com/berita-ekonomi-bisnis/d-5245554/gaji-rp-5-jutabulan-masih-bisa-nabung-dan-investasi-nggak-ya">Gaji Rp 5 Juta/Bulan, Masih Bisa Nabung dan Investasi Nggak Ya?</a></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <p><br />
                Tekanan psikologis yang dialami oleh generasi ini bisa terjadi karena orang tua atau generasi tua tidak menyiapkan masa tuanya dengan baik. Dalam hal ini, bukan hanya kehidupan finansial yang perlu dipersiapkan, tetapi juga menjaga kehidupan kesehatan.</p>
                
                <p>Fakta kemudian membuktikan bahwa kebiasaan ini sebenarnya juga sudah menjadi turunan di Indonesia. Seperti apa contoh tersebut terjadi di Indonesia? Contoh klasik yang sering terjadi adalah, orang tua ikut anaknya untuk biaya hidup, kemungkinan besar karena uang pensiunannya sudah habis atau bahkan tidak mempersiapkan uang pensiun sama sekali.<br />
                <br />
                Sementara itu sebaliknya, alasan anak menanggung orang tua adalah karena anak ingin berbakti kepada orang tua dan memang tidak ada yang salah dengan hal tersebut. Hanya yang jadi permasalahan adalah ketika kita sebagai anak tidak mempunyai kemampuan finansial untuk membiayai kehidupan orang tua dan juga keluarganya sendiri.<br />
                <br />
                Harap diingat bahwa tidak semua anak mempunyai pekerjaan dengan posisi yang bagus dengan penghasilan yang cukup.</p>
                
                <table>
                    <tbody>
                        <tr>
                            <td>
                            <p><strong>Baca juga: </strong><a href="https://finance.detik.com/perencanaan-keuangan/d-5216404/mau-pensiun-sejahtera-ikuti-4-tips-ini">Mau Pensiun Sejahtera? Ikuti 4 Tips Ini</a></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <p><br />
                Apabila anak tersebut memiliki dana yang berkecukupan maka menanggung biaya hidup orang tua dan keluarga mereka sendiri tidak akan menjadi masalah, akan tetapi rata-rata dana yang mereka miliki relative kurang mencukupi alias mepet sehingga ini yang akan menjadi permasalahan di kemudian hari. Biaya hidup yang mepet atau bahkan kurang akan menimbulkan hutang untuk pembiayaan hidup keperluan rumah tangga sehari-hari.</p>
                
                <p>Jika masih menopang kehidupan orang tua dan keluarga sendiri (beserta anak tentunya), sudah saatnya kita mulai melakukan sesuatu untuk bisa memutus mata rantai generasi sandwich ini. Hal ini harus dimulai dari diri sendiri yaitu dengan berupa mencari penghasilan tambahan. Nah, seperti apa sih tip dan trik mengatur keuangan ini? Yuk kita bahas bersama</p>',
                'image' => '1605334779.jpeg',
                'thumbnail' => '1605334779.jpeg',
            ],
            [
                'category_id' => 2,
                'created_by' => 1,
                'title' => 'Hampir Dua Tahun Berturut-turut, Nintendo Switch Jadi Konsol Terlaris di AS',
                'slug' => 'hampir-dua-tahun-berturut-turut,-nintendo-switch-jadi-konsol-terlaris-di-as',
                'short_description' => '<p><strong>Jakarta</strong> - Dua tahun ini bisa dibilang tahunnya <a href="https://www.detik.com/tag/nintendo-switch">Nintendo Switch</a>, setidaknya di AS. Pasalnya konsol tersebut menurut Nintendo adalah konsol terlaris selama 23 bulan berturut-turut, alias hampir dua tahun.</p>

                <p>Sementara mengacu pada data dari NPD Group, yang menyebut pada Oktober lalu ada 735 ribu Nintendo Switch dan Switch Lite terjual di AS, demikian dikutip detikINET dari <em>The Verge</em>, Sabtu (14/11/2020).</p>',
                'content' => '<p><strong>Jakarta</strong> -</p>

                <p>Dua tahun ini bisa dibilang tahunnya <a href="https://www.detik.com/tag/nintendo-switch">Nintendo Switch</a>, setidaknya di AS. Pasalnya konsol tersebut menurut Nintendo adalah konsol terlaris selama 23 bulan berturut-turut, alias hampir dua tahun.</p>
                
                <p>Sementara mengacu pada data dari NPD Group, yang menyebut pada Oktober lalu ada 735 ribu Nintendo Switch dan Switch Lite terjual di AS, demikian dikutip detikINET dari <em>The Verge</em>, Sabtu (14/11/2020).</p>
                
                <p>Angka penjualan ini lebih tinggi 136% dibanding tahun sebelumnya, dan sejauh ini sudah ada 22,5 juta unit Nintendo Switch yang terjual di AS. Sementara secara global, ada lebih 68 juta unit yang sudah terjual.</p>
                
                <p><ins><img alt="" src="https://wtf2.forkcdn.com/www/delivery/lg.php?bannerid=0&amp;campaignid=0&amp;zoneid=4562&amp;loc=https%3A%2F%2Finet.detik.com%2Fgames-news%2Fd-5254748%2Fhampir-dua-tahun-berturut-turut-nintendo-switch-jadi-konsol-terlaris-di-as&amp;referer=https%3A%2F%2Finet.detik.com%2F%3Ftag_from%3Dframebar%26_ga%3D2.214425298.475490776.1605325775-179159330.1601346483&amp;cb=6f16969107" style="height:0px; width:0px" /></ins></p>
                
                <p>&nbsp;</p>
                
                <p>&quot;Kami sangat bersemangat melihat momentum ini,&quot; ujar Nick Chavez, SVP of sales and marketing di Nintendo of America.</p>
                
                <table>
                    <tbody>
                        <tr>
                            <td>
                            <p><strong>Baca juga: </strong><a href="https://inet.detik.com/games-news/d-5234603/sebulan-pemasukan-genshin-impact-tembus-rp-36-triliun">Sebulan, Pemasukan Genshin Impact Tembus Rp 3,6 Triliun</a></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <p>Peningkatan penjualan pada Oktober ini menurut Chavez terjadi karena dua faktor utama. Salah satunya adalah pasokan yang lebih baik. Meski seringkali sulit untuk menemukan Switch di rak-rak toko.</p>
                
                <p>Langkanya Switch itu disebabkan oleh meningkatnya permintaan akibat kombinasi pandemi Corona dan kesuksesan game Animal Crossing: New Horizon.</p>
                
                <p>&quot;Pandemi covid ini menciptakan banyak kesempatan untuk berdiam diri di rumah, lebih banyak waktu untuk bermain di rumah. Kami melihat permintaan yang tak diduga-duga terhadap <a href="https://www.detik.com/tag/nintendo-switch">Nintendo Switch</a> selama tahun ini,&quot; tambah Chavez.</p>
                
                <p>Selain pandemi dan game Animal Crossing: New Horizon, meningkatnya penjualan Switch disebabkan karena adanya pertumbuhan jumlah pembelian oleh perempuan dan untuk keluarga.</p>
                
                <table>
                    <tbody>
                        <tr>
                            <td>
                            <p><strong>Baca juga: </strong><a href="https://inet.detik.com/games-news/d-5241606/kontroler-ps5-bisa-dipakai-untuk-nintendo-switch">Kontroler PS5 Bisa Dipakai untuk Nintendo Switch</a></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <p>&quot;Pertumbuhan kami tahun ini banyak yang terjadi karena perempuan bermain Nintendo Switch, semakin banyak anak-anak dan remaja bermain Nintendo Switch, dan juga orang tuanya,&quot; tambah Chavez.</p>
                
                <p>Ia pun menyebut adanya preferensi yang sangat kuat untuk sistem flagship dengan harga USD 299.</p>
                
                <p><em>Bagaimana persaingan <a href="https://www.detik.com/tag/nintendo-switch">Nintendo Switch</a> dengan <a href="https://www.detik.com/tag/ps5">PS5</a> dan Xbox Series X?</em></p>',
                'image' => '1605334873.jpeg',
                'thumbnail' => '1605334873.jpeg',
            ],
            [
                'category_id' => 2,
                'created_by' => 1,
                'title' => 'Daftar Game PS5 yang Sudah Diumumkan dan Harganya',
                'slug' => 'daftar-game-ps5-yang-sudah-diumumkan-dan-harganya',
                'short_description' => '<p><strong>Jakarta</strong> - PS5 meluncur dengan sederet game eksklusif. Inilah daftar game PS5 yang sudah dinanti-nanti dan puluhan game lainnya.</p>

                <p><a href="https://www.detik.com/tag/ps5">PlayStation 5 </a>sudah meluncur di berbagai negara mulai Kamis kemarin. Meski gamer di Indonesia masih harus menunggu hingga Januari 2021, kalian sudah bisa merencanakan game apa saja yang ingin dibeli dan dimainkan.</p>',
                'content' => '<p><strong>Jakarta</strong> -</p>

                <p>PS5 meluncur dengan sederet game eksklusif. Inilah daftar game PS5 yang sudah dinanti-nanti dan puluhan game lainnya.</p>
                
                <p><a href="https://www.detik.com/tag/ps5">PlayStation 5 </a>sudah meluncur di berbagai negara mulai Kamis kemarin. Meski gamer di Indonesia masih harus menunggu hingga Januari 2021, kalian sudah bisa merencanakan game apa saja yang ingin dibeli dan dimainkan.</p>
                
                <p>Sony sudah mengumumkan beberapa game eksklusif untuk PS5, baik dari yang first-party atau dari third-party. Karena PS5 mendukung backwards compatibility, game-game PS4 kesayangan kalian juga bisa dimainkan di PS5.</p>
                
                <p><ins><img alt="" src="https://wtf2.forkcdn.com/www/delivery/lg.php?bannerid=0&amp;campaignid=0&amp;zoneid=4562&amp;loc=https%3A%2F%2Finet.detik.com%2Fgames-news%2Fd-5254726%2Fdaftar-game-ps5-yang-sudah-diumumkan-dan-harganya%3Ftag_from%3Dwp_nhl_1&amp;referer=https%3A%2F%2Finet.detik.com%2F%3Ftag_from%3Dframebar%26_ga%3D2.214425298.475490776.1605325775-179159330.1601346483&amp;cb=f74c0d15a7" style="height:0px; width:0px" /></ins></p>
                
                <p>&nbsp;</p>
                
                <table>
                    <tbody>
                        <tr>
                            <td>
                            <p><strong>Baca juga: </strong><a href="https://inet.detik.com/games-news/d-5253362/ps5-meluncur-sony-ungkap-nasib-ps4">PS5 Meluncur, Sony Ungkap Nasib PS4</a></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <p>Selain game-game yang sudah diumumkan, PS5 juga akan kedatangan banyak game baru yang akan diluncurkan dalam beberapa bulan ke depan. Berikut daftar game PS5 yang bisa kalian mainkan beserta harganya, seperti dikutip <strong>detikINET </strong>dari The Verge, Sabtu (14/11/2020).</p>
                
                <h3>Game eksklusif PS5</h3>
                
                <p><a href="https://www.detik.com/tag/sony">Sony </a>telah mengumumkan enam game first-party yang bisa langsung diunduh dan dimainkan setelah <a href="https://www.detik.com/tag/ps5">PS5 </a>meluncur. Game-game tersebut adalah:</p>
                
                <p>- Astro&#39;s Playroom preinstal di PS5</p>
                
                <p>- Demon&#39;s Souls Rp 1.029.000</p>
                
                <p>- Destruction AllStars Rp 1.029.000</p>
                
                <p>- Marvel&#39;s Spider-Man: Miles Morales Rp 729.000</p>
                
                <p>- Marvel&#39;s Spider-Man: Miles Morales Ultimate Edition Rp 1.029.000</p>
                
                <p>- Sackboy A Big Adventure Rp 879.000</p>
                
                <p>Jika kalian belum ingin membeli PS5, jangan khawatir karena beberapa game di atas bisa dimainkan di PS4. Spider Man: Miles Morales dan Sackboy: A Big Adventure tersedia di PS4, dan Sony berencana untuk merilis Horizon Forbidden West di PS4.</p>
                
                <p>Selain game first-party dari Sony akan ada beberapa game third-party yang tersedia untuk <a href="https://www.detik.com/tag/ps5">PS5 </a>antara lain:</p>
                
                <p>- Assassin&#39;s Creed Valhalla</p>
                
                <p>- Borderlands 3</p>
                
                <p>- Bugsnax (gratis dengan PS Plus)</p>
                
                <p>- Call of Duty: Black Ops Cold War (meluncur 13 November)</p>
                
                <p>- Dead by Daylight</p>
                
                <p>- Devil May Cry 5: Special Edition</p>
                
                <p>- Dirt 5</p>
                
                <p>- Fortnite</p>
                
                <p>- Godfall</p>
                
                <p>- Goonya Fighter</p>
                
                <p>- King Oddball</p>
                
                <p>- Maneater</p>
                
                <p>- NBA 2K21</p>
                
                <p>- No Man&#39;s Sky</p>
                
                <p>- Observer: System Redux</p>
                
                <p>- Overcooked</p>
                
                <p>- The Pathless</p>
                
                <p>- Planet Coaster</p>
                
                <p>- Warhammer: Chaosbane Slayer Edition</p>
                
                <p>- Watch Dogs Legion</p>
                
                <p>- WRC 9 FIA World Rally Championship</p>
                
                <p>&nbsp;</p>
                
                <p><em>Halaman selanjutnya: PlayStation Plus Collection...</em></p>',
                'image' => '1605334976.jpeg',
                'thumbnail' => '1605334976.jpeg',
            ],
            [
                'category_id' => 2,
                'created_by' => 1,
                'title' => 'Aplikasi All-in-One Bikin Kerja Lebih Praktis, Kok Bisa?',
                'slug' => 'aplikasi-all-in-one-bikin-kerja-lebih-praktis,-kok-bisa?',
                'short_description' => '<p><strong>Jakarta</strong> - Di tengah pandemi COVID-19 saat ini, banyak perusahaan yang memberlakukan kebijakan <em>work from home</em> (WFH). Meskipun terkesan santai karena bekerja di rumah, namun siapa sangka kalau WFH juga memberikan banyak kendala.</p>

                <p>&nbsp;</p>
                
                <p>Melansir nisbusinessinfo, ada beberapa kendala yang bisa muncul akibat WFH antara lain kesulitan memantau pekerjaan, sulit berkomunikasi, menurunnya produktivitas, hingga pekerjaan yang justru malah bertambah. Akibatnya, kerja pun menjadi kurang efisien dan sulit menerapkan <em>work life balance</em>.</p>',
                'content' => '<p><strong>Jakarta</strong> - Di tengah pandemi COVID-19 saat ini, banyak perusahaan yang memberlakukan kebijakan <em>work from home</em> (WFH). Meskipun terkesan santai karena bekerja di rumah, namun siapa sangka kalau WFH juga memberikan banyak kendala.</p>

                <p>&nbsp;</p>
                
                <p>Melansir nisbusinessinfo, ada beberapa kendala yang bisa muncul akibat WFH antara lain kesulitan memantau pekerjaan, sulit berkomunikasi, menurunnya produktivitas, hingga pekerjaan yang justru malah bertambah. Akibatnya, kerja pun menjadi kurang efisien dan sulit menerapkan <em>work life balance</em>.</p>
                
                <p>Untuk mengatasi hal ini, ada banyak hal yang bisa dilakukan, salah satunya dengan memanfaatkan platform digital dan teknologi informasi (TI). Melansir Forbes, dari 1.000 lebih grup survei, 76% karyawan menyebut platform digital membuatnya lebih produktif saat bekerja.</p>
                
                <p>Sementara 53% pekerja mengatakan platform digital membuatnya lebih sukses dan sepertiga mengatakan membuatnya menjadi lebih pintar.</p>
                
                <p>Lalu, bagaimana cara platform digital bisa meningkatkan pekerjaan sehari-hari? Sebagaimana diketahui, platform digital umumnya memiliki berbagai aplikasi yang bisa memudahkan pekerja mulai dari mengatur jadwal, <em>meeting</em>, koordinasi dengan divisi lain, dan lainnya. Dengan demikian pekerjaan menjadi lebih efisien.</p>
                
                <p>Sebagai salah satu <em>collaboration tools</em>, Lark hadir mendukung produktivitas dan efisiensi pekerja melalui rangkaian aplikasi mulai dari Lark Messenger, Lark Docs, Lark Calendar, Lark Video Conferencing, Lark Mail dan Lark Workplace.</p>
                
                <p>Adapun aplikasi ini terintegrasi dalam sebuah aplikasi tunggal pada Mac, PC, iOS, dan Android, sehingga dapat mempermudah pekerjaan sehari-hari. Berikut beberapa kemudahan yang Lark hadirkan untuk berbagai aktivitas pekerjaan sehari-hari.</p>
                
                <p><strong>Mengatur Jadwal Harian </strong></p>
                
                <p>Sebagai pekerja, tentunya dalam satu hari Anda akan memiliki berbagai macam jadwal mulai dari bangun tidur, bekerja, hingga semua pekerjaan terselesaikan. Banyaknya aktivitas dalam satu hari bisa saja memungkinkan Anda lupa untuk melewatkan 1-2 aktivitas.</p>
                
                <p>Dalam hal ini, Lark Calendar akan sangat membantu Anda sebagai pengingat seluruh aktivitas dalam sehari, seminggu, sebulan, atau bahkan setahun. Di Lark Calendar, Anda juga bisa mengatur berbagai jadwal pertemuan baik itu dengan klien atau rekan kerja.</p>
                
                <p>Selain itu, Lark Calender juga menampilkan kalender tim dan beberapa kalender secara berdampingan sehingga baik Anda dan rekan kerja lainnya bisa mudah menentukan waktu deadline baik untuk diri sendiri atau tim. Hal ini tentunya akan meminimalisir Anda untuk melewatkan pekerjaan atau aktivitas di hari tersebut.</p>
                
                <table align="center">
                    <tbody>
                        <tr>
                            <td><img alt="adv" src="https://akcdn.detik.net.id/community/media/visual/2020/11/13/adv-1.png?w=595" />
                            <p>Foto: Dok. Lark</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <p><strong>Meeting</strong></p>
                
                <p>Pekerja biasanya sangat identik dengan aktivitas <em>meeting</em>. Di tengah pandemi saat ini, Lark Video Conferencing bisa jadi solusi untuk melakukan <em>meeting</em> bersama klien, tim, atau anggota lainnya. Bahkan bagi Anda yang memiliki klien dari luar negeri, Lark Video Conferencing menghadirkan fitur terjemahan langsung sehingga pekerjaan tentunya akan jauh lebih efisien.</p>
                
                <p>Ingin menggelar meeting secara bersamaan dengan berbagai divisi? Lark Video Conferencing sangat memungkinkan Anda untuk menggelar <em>meeting</em> online dengan menit tak terbatas dan peserta hingga 100 orang. Ada juga fitur Magic Share yang membebaskan peserta berbagi dan mengedit file selama <em>meeting</em> berlangsung.</p>
                
                <p><strong>Mengerjakan Tugas</strong></p>
                
                <p>Saat memulai kerja tentunya akan ada banyak pekerjaan yang perlu diselesaikan. Dalam hal ini, Lark Docs akan membantu Anda menyelesaikan pekerjaan dengan lebih efektif dan efisien. Dengan fitur <em>sharing docs</em>, tentunya Anda akan lebih mudah mengakses dokumen serta melakukan perubahan secara <em>real time</em> terhadap berbagai pekerjaan.</p>
                
                <p>Bukan hanya itu, Lark Docs juga memiliki fitur komentar yang bisa digunakan dalam Lark Doc atau Lark Sheets sehingga memudahkan Anda untuk memberi atau mengakses <em>feedback</em> terhadap seluruh tugas-tugas Anda.</p>
                
                <p>Sedangkan untuk perusahaan besar, Anda dapat memanfaatkan fitur Lark Workplace dengan mengintegrasikan pihak ketiga seperti Jira, Asana, hingga Salesforce. Aplikasi ini akan membebaskan Anda untuk membuat aplikasi dan bot di platform terbuka sesuai dengan pekerjaan dan kebutuhan.</p>
                
                <table align="center">
                    <tbody>
                        <tr>
                            <td><img alt="adv" src="https://akcdn.detik.net.id/community/media/visual/2020/11/13/adv-2.png?w=619" />
                            <p>Foto: Dok. Lark</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <p><strong>Koordinasi Pekerjaan</strong></p>
                
                <p>Aktivitas pekerja yang pasti selalu dilakukan setiap harinya adalah berkoordinasi dengan rekan kerja, tim, atau atasan soal pekerjaan. Biar lebih efisien, cobalah untuk menggunakan Lark Messenger dan Lark Mail saat berkomunikasi.</p>
                
                <p>Lark Messenger dapat menampung hingga 5.000 anggota serta dilengkapi fitur <em>unlimited searchable chat history</em> sehingga dapat memaksimalkan komunikasi antar anggota dalam grup. Lewat aplikasi ini, anggota juga dapat mengecek jadwal anggota lain, menelepon, hingga mengirim dokumen. Uniknya lagi, Lark Messenger juga dapat menerjemahkan pesan otomatis lebih dari 100 bahasa termasuk Inggris, Cina, Jepang, dan Thailand.</p>
                
                <p>Sedangkan Lark Mail berfungsi untuk mempermudah koordinasi pekerjaan secara keseluruhan melalui <em>email, messenger</em>, dan <em>drive</em>, yang berkapasitas hingga 500 GB. Lewat Lark Mail, Anda juga bisa membagikan email ke obrolan grup dengan cepat dan mudah.</p>
                
                <p>Nah, itulah beberapa kemudahan yang bisa didapatkan para pekerja dari rangkaian aplikasi Lark. Dengan begitu seluruh aktivitas harian mulai dari bangun di pagi hari hingga selesai bekerja bisa dilakukan secara lebih efisien. Berminat menggunakan Lark? Untuk informasi lebih lanjut bisa dilihat <a href="http://bit.ly/LarkDetik" target="_blank">di sini</a>. <strong>(adv/adv)</strong></p>',
                'image' => '1605335059.jpeg',
                'thumbnail' => '1605335059.jpeg',
            ],
            [
                'category_id' => 2,
                'created_by' => 1,
                'title' => 'Tidak Ada YouTube Rewind Tahun Ini, Kenapa?',
                'slug' => 'tidak-ada-youtube-rewind-tahun-ini,-kenapa',
                'short_description' => '<p><strong>Jakarta</strong> -</p>

                <p>Sejak tahun 2010, <a href="https://www.detik.com/tag/youtube">YouTube </a>selalu menutup tahun dengan menayangkan video kompilasi <a href="https://www.detik.com/tag/youtube-rewind">YouTube Rewind</a>. Tapi untuk tahun ini video YouTube Rewind akan ditiadakan.</p>
                
                <p>Video Rewind yang diunggah tiap tahunnya menyorot momen, kreator dan tren terbesar di YouTube selama setahun terakhir. Namun, karena tahun 2020 adalah tahun yang berbeda dibanding tahun-tahun sebelumnya, YouTube merasa kurang cocok jika meluncurkan video Rewind tahun ini.</p>',
                'content' => '<p><strong>Jakarta</strong> -</p>

                <p>Sejak tahun 2010, <a href="https://www.detik.com/tag/youtube">YouTube </a>selalu menutup tahun dengan menayangkan video kompilasi <a href="https://www.detik.com/tag/youtube-rewind">YouTube Rewind</a>. Tapi untuk tahun ini video YouTube Rewind akan ditiadakan.</p>
                
                <p>Video Rewind yang diunggah tiap tahunnya menyorot momen, kreator dan tren terbesar di YouTube selama setahun terakhir. Namun, karena tahun 2020 adalah tahun yang berbeda dibanding tahun-tahun sebelumnya, YouTube merasa kurang cocok jika meluncurkan video Rewind tahun ini.</p>
                
                <table>
                    <tbody>
                        <tr>
                            <td>
                            <p><strong>Baca juga: </strong><a href="https://inet.detik.com/cyberlife/d-5250792/mahir-bicara-medok-youtuber-korea-jang-hansol-bakal-jadi-dosen">Mahir Bicara &#39;Medok&#39;, Youtuber Korea Jang Hansol Bakal Jadi Dosen</a></p>
                
                            <p><ins><img alt="" src="https://wtf2.forkcdn.com/www/delivery/lg.php?bannerid=0&amp;campaignid=0&amp;zoneid=4562&amp;loc=https%3A%2F%2Finet.detik.com%2Fcyberlife%2Fd-5254680%2Ftidak-ada-youtube-rewind-tahun-ini-kenapa%3Ftag_from%3Dwp_nhl_2&amp;referer=https%3A%2F%2Finet.detik.com%2F%3Ftag_from%3Dframebar%26_ga%3D2.214425298.475490776.1605325775-179159330.1601346483&amp;cb=3ce6c09212" style="height:0px; width:0px" /></ins></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <p>&quot;Kami tahu bahwa banyak hal baik yang terjadi di tahun 2020 diciptakan oleh kalian semua,&quot; tulis YouTube dalam pernyataan yang diunggah di Twitter, seperti dikutip <strong>detikINET </strong>dari The Verge, Sabtu (13/11/2020).</p>
                
                <p>&quot;Kalian menemukan cara untuk menyenangkan orang lain, membantu mereka bertahan, dan membuat mereka tertawa. Kalian membuat tahun yang sulit menjadi jauh lebih baik,&quot; sambungnya.</p>
                
                <p>&nbsp;</p>
                
                <p>&nbsp;</p>
                
                <p>&nbsp;</p>
                
                <p>&nbsp;</p>
                
                <p>Tahun 2020 seharusnya menjadi tahun ke-10 anak perusahaan Google ini meluncurkan video Rewind. Tapi tahun ini justru menjadi tahun pertama YouTube Rewind absen sejak pertama kali ditayangkan 10 tahun yang lalu.</p>
                
                <p>Netizen di Twitter memiliki reaksi yang berbeda terhadap pengumuman <a href="https://www.detik.com/tag/youtube">YouTube </a>yang meniadakan Rewind tahun ini. Ada yang menganggap ini keputusan yang tepat, ada juga yang mengatakan ini seharusnya menjadi momen bagi YouTube untuk menyorot hal-hal positif yang terjadi di tahun 2020.</p>
                
                <p>&nbsp;</p>
                
                <p>&nbsp;</p>
                
                <p>&nbsp;</p>
                
                <p>&nbsp;</p>
                
                <p>&nbsp;</p>
                
                <p>&nbsp;</p>
                
                <p>&nbsp;</p>
                
                <table>
                    <tbody>
                        <tr>
                            <td>
                            <p><strong>Baca juga: </strong><a href="https://inet.detik.com/cyberlife/d-5251953/youtube-down-di-seluruh-dunia-ini-penjelasannya">YouTube Down di Seluruh Dunia, Ini Penjelasannya</a></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <p>Video YouTube Rewind yang diisi oleh selebriti dan YouTuber wahid biasanya ditonton hingga ratusan juta kali setiap tahunnya. Meski banyak ditonton, banyak pengguna lainnya yang menganggap video ini sedikit mengecewakan.</p>
                
                <p>Seperti contonya video YouTube Rewind 2018 yang dibintangi puluhan YouTuber dan selebriti, dari Gen Halilintar sampai Will Smith. Meski telah ditonton lebih dari 200 juta kali, video ini juga menjadi video paling tidak disukai di YouTube dengan 18 juta dislike.</p>
                
                <p>Belum diketahui apakah <a href="https://www.detik.com/tag/youtube">YouTube </a>akan menghadirkan kembali video Rewind di tahun 2021.</p>',
                'image' => '1605335312.jpeg',
                'thumbnail' => '1605335312.jpeg',
            ],
            [
                'category_id' => 2,
                'created_by' => 1,
                'title' => 'Mager Tapi Digaji Rp 127,3 Juta, Mau?',
                'slug' => 'mager-tapi-digaji-rp-127,3-juta,-mau?',
                'short_description' => '<p><strong>Jakarta</strong> -</p>

                <p>Browsing atau berselancar di<a href="https://www.detik.com/tag/internet"> internet</a> sudah menjadi bagian dari hidup di era digital seperti saat ini, baik untuk bekerja maupun mencari informasi. Namun, maukah kalian dibayar USD 9.000 atau Rp 127,3 juta (kurs USD 1 = Rp 14.215) hanya untuk browsing saja?</p>
                
                <p>Sebuah pekerjaan yang mudah dan tentunya sebuah tawaran gaji yang cukup membuat kantong <em>detikers</em> menjadi tebal mendadak. Begitu lowongan pekerjaan alias loker yang disediakan perusahaan teknologi asal Norwegia, <a href="https://detik.com/tag/opera">Opera</a>.</p>',
                'content' => '<p><strong>Jakarta</strong> -</p>

                <p>Browsing atau berselancar di<a href="https://www.detik.com/tag/internet"> internet</a> sudah menjadi bagian dari hidup di era digital seperti saat ini, baik untuk bekerja maupun mencari informasi. Namun, maukah kalian dibayar USD 9.000 atau Rp 127,3 juta (kurs USD 1 = Rp 14.215) hanya untuk browsing saja?</p>
                
                <p>Sebuah pekerjaan yang mudah dan tentunya sebuah tawaran gaji yang cukup membuat kantong <em>detikers</em> menjadi tebal mendadak. Begitu lowongan pekerjaan alias loker yang disediakan perusahaan teknologi asal Norwegia, <a href="https://detik.com/tag/opera">Opera</a>.</p>
                
                <p>Opera yang merupakan penyedia layanan peramban pesaing <a href="https://www.detik.com/tag/chrome">Chrome</a>, <a href="https://www.detik.com/tag/mozilla-firefox?tag_from=firefox">Mozilla Firefox</a> ataupun <a href="https://www.detik.com/tag/microsoft-edge">Microsoft Edge</a>, sedang mencari orang yang dinilai cocok mengisi pekerjaan yang mereka sebut dengan &#39;Personal Browser&#39;.</p>
                
                <p><ins><img alt="" src="https://wtf2.forkcdn.com/www/delivery/lg.php?bannerid=0&amp;campaignid=0&amp;zoneid=4562&amp;loc=https%3A%2F%2Finet.detik.com%2Fcyberlife%2Fd-5247167%2Fmager-tapi-digaji-rp-1273-juta-mau&amp;referer=https%3A%2F%2Finet.detik.com%2F%3Ftag_from%3Dframebar%26_ga%3D2.214425298.475490776.1605325775-179159330.1601346483&amp;cb=2a9514c2a0" style="height:0px; width:0px" /></ins></p>
                
                <p>&nbsp;</p>
                
                <p>&nbsp;</p>
                
                <table>
                    <tbody>
                        <tr>
                            <td>
                            <p><strong>Baca juga: </strong><a href="https://inet.detik.com/games-news/d-4584400/opera-bikin-browser-khusus-gamer">Opera Bikin Browser Khusus Gamer</a></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <p>&nbsp;</p>
                
                <p>Seperti dikutip dari<em> UPI</em>, Senin (9/11/2020) tugas &#39;Personal Browser&#39; itu hanya melakukan aktivitas online, misalnya berburu meme, menonton video bayi hewan yang menggemaskan nan lucu, maupun meneliti topik yang tidak biasa sambil live streaming di saluran media sosial Opera.</p>
                
                <p>Pekerjaan &#39;Personal Browser&#39; ini hanya berlangsung dua minggu saja. Akan tetapi, apabila menemukan orang yang cocok dengan pekerjaan tersebut, Opera menggajinya sebesar USD 9.000 atau Rp 127,9 juta.</p>
                
                <p><em>Nah</em>, kalau kalian tertarik, bisa melamar lowongan pekerjaan ini. Syaratnya cukup mudah, cukup dengan merekam diri berupa video yang durasinya 15-60 detik. Di dalam video itu, kalian ekspresikan momen menjelajahi dunia maya yang dinilai paling relevan dalam hidup.</p>
                
                <p>&quot;Ini mungkin tampak seperti lelucon, tapi sebenarnya tidak. Kami memang mempekerjakan seseorang hanya untuk menjelajahi web dan benar-benar dibayar untuk itu,&quot; ungkap Direktur Produk <a href="https://detik.com/tag/opera">Opera</a> Maciej Kocemba.</p>
                
                <p>Opera menyebutkan lowongan pekerjaan terbuka bagi siapa saja dan di mana pun orang itu berada. Adapun lowongan ini tersedia sampai tanggal 13 November 2020. Tertarik <em>detikers</em> untuk mencobanya?</p>',
                'image' => '1605335538.jpeg',
                'thumbnail' => '1605335538.jpeg',
            ],
        ]);
    }
}
