## Installation
Clone Repository
```bash
git clone https://ado_pabianko@bitbucket.org/ado_pabianko/fas-id.git
```

copy file .env.example
```bash
cp -R .env.example .env
```

run composer install
```bash
composer install
```

run database migration & database seeder
```bash
php artisan db:migrate 
php artisan db:seed
```

run server
```bash
php artisan serve
```

access frontent http://localhost:8000  
access cms http://localhost:8000/admin  
default user :  
    - email : admin@admin.com  
    - password : secret  

## Installation via Docker
Clone Repository
```bash
git clone https://ado_pabianko@bitbucket.org/ado_pabianko/fas-id.git
```

copy file .env.example
```bash
cp -R .env.example .env
```

run composer install
```bash
composer install
```

create docker network
```bash
docker network create fas_id_network
```

run docker compose
```bash
docker-compose up -d
```
run database migration & database seeder
```bash
docker-compose exec fas-id-app php artisan migrate
docker-compose exec fas-id-app php artisan db:seed
```
access frontent http://localhost:8000  
access cms http://localhost:8000/admin  
default user :  
    - email : admin@admin.com  
    - password : secret  

## Screenshot
Home Page
![Home](Home.png?raw=true "Home")

Dashboard Page
![Dashboard](Dashboard.png?raw=true "Dashboard")